## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Mon Apr 05 2021 14:49:22 GMT+0300 (GMT+03:00)|
|**App Generator**<br>@sap/generator-fiori|
|**App Generator Version**<br>1.1.7|
|**Generation Platform**<br>Visual Studio Code|
|**Floorplan Used**<br>1worklist|
|**Service Type**<br>SAP System (ABAP On Premise)|
|**Service URL**<br>http://172.0.2.3:8000/sap/opu/odata/sap/ZTM_TASK_MANAGEMENT_SRV
|**Module Name**<br>zays_prj_info|
|**Application Title**<br>Proje Bilgileri|
|**Namespace**<br>com.arete|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>Latest|
|**Enable Telemetry**<br>True|

## zays_prj_info

A Fiori application.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```


#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


