sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	'sap/m/library',
	"../model/formatter",
	'sap/ui/model/Sorter'
        ], function(BaseController, JSONModel, History, Filter, FilterOperator, mobileLibrary, formatter, Sorter) {
	"use strict";
        
	return BaseController.extend("com.arete.zaysprjinfo.controller.Object", {
        
	    formatter: formatter,
        
	    onInit: function() {
	        var  areteLogoImage = jQuery.sap.getModulePath("com.arete.zaysprjinfo"); 
	        var oViewModel = new JSONModel({
		objectTableTitle: this.getResourceBundle().getText("objectTableTitle"),
		PersonSet: [],
		SystemSet: [],
		TableData: [],
		AreteLogo:areteLogoImage,
		ProjectIdText: "",
		Email:[],
	        });
	        this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);
	        this.setModel(oViewModel, "objectView");
        
	    },
	    onUpdateFinished : function (oEvent) {
		     
		var sTitle,
		          oTable = oEvent.getSource(),
		          iTotalItems = oEvent.getParameter("total");
		if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
		          sTitle = this.getResourceBundle().getText("objectTableTitleCount", [iTotalItems]);
		} 
else
{
		          sTitle = this.getResourceBundle().getText("objectTableTitle");
		}
		  this.getModel("objectView").setProperty("/objectTableTitle", sTitle);
	  },
	    _onObjectMatched: function(oEvent) {
	        var aSystemFilter = [],
		aPersonFilter = [],
		sObjectId = oEvent.getParameter("arguments").projectId, //project Id mi aldığım yer.
		oViewModel = this.getModel("objectView"),
		oDataModel = this.getModel();
	        oViewModel.setProperty("/ProjectIdText", sObjectId);
        
	        aSystemFilter.push(new Filter("ProjectId", FilterOperator.EQ, sObjectId));
	        oDataModel.read("/SystemsSet", {
		filters: aSystemFilter,
		success: function(data) {
			 
		    oViewModel.setProperty("/SystemSet", data.results);

		},
		error: function() {
			 
			
		},
	        });

	        aPersonFilter.push(new Filter("Projectid", FilterOperator.EQ, sObjectId))
	        oDataModel.read("/PPersonListSet", {
		filters: aPersonFilter,
        
		success: function(data) {
			 
			
			oViewModel.setProperty("/PersonSet",data.results);
			 
		          for(var i=0;i<data.results.length;i++)
		          {
			          var a=data.results[i].Email.toLowerCase();

			         
			          oViewModel.setProperty(("/PersonSet/" + i + "/Email"), a);			           
	         		 }
	          		 
		},
		
		error: function() {
			 
			oViewModel.setProperty("/PersonSet",[]);
		},
		
	        });

	        
	    },
	    handleEmailPress: function (oEvent) {
		     
		    var email=oEvent.getSource().getText();
		    sap.m.URLHelper.triggerEmail(email);
		} ,
		
		getGroup: function (oContext){
			var sKey = oContext.getProperty("Module");
			return {
			    key: sKey,
			    title: sKey || "Modül bulunamadı."
			};
		        },
		        getGroupHeader: function (oGroup){
			return new sap.m.GroupHeaderListItem({
			    title: oGroup.title,
			    upperCase: false
			});
		        } 
	});
        
        });
        