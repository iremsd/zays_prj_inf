sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
        ], function(BaseController, JSONModel, formatter, Filter, FilterOperator) {
	"use strict";
        
	return BaseController.extend("com.arete.zaysprjinfo.controller.Worklist", {
        
	    formatter: formatter,
        
	    onInit: function() {
	        var oViewModel = new JSONModel({
		
	        });
	        this.setModel(oViewModel, "worklistView");
	        
	    },
	    onPress: function(oEvent) {
        
	        this._showObject(oEvent.getSource());
	    },
	    _showObject: function(oItem) {
        
	        this.getRouter().navTo("object", {
		projectId: oItem.getBindingContext().getProperty("ProjectId")
	        });
	    },
	    onFilterSearch:function(oEvent)
	    {
		
		/* var aFilters = [],
		
		 sQuery =  oEvent.getParameter("query");
		aFilters.push(new Filter("AnamuhatapA", FilterOperator.Contains, sQuery)); */
		  var  sQuery =  oEvent.getParameter("query"),
		  aFilters= [
			new Filter("AnamuhatapA", FilterOperator.Contains, sQuery),
			new Filter("AnamuhatapM", FilterOperator.Contains, sQuery),
			new Filter("ProjectText", FilterOperator.Contains, sQuery),
			];
		  

		// güncellen table ı bağlıyoruz
		var oTable = this.byId("mTableId"),
		 oBinding = oTable.getBinding("items");
		oBinding.filter(new Filter(aFilters,false)); 
		
	    },
	    handleEmailPress: function (oEvent) {
		  
		var email=oEvent.getSource().getText();
		sap.m.URLHelper.triggerEmail(email);
	        }
        
	});
        });